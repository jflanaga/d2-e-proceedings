# Reproducible Research: Strategies, Tools, and Workflows

This repository contains the text and code for the following article: 

(Add title and publication information)

The paper, along with accompanying videos, is meant to demonstrate how to make it possible for others to easily and conveniently reproduce the statistics, figures, and tables that appear in a published work. 

The data used in this project comes from Wolk, Christoph, Joan Bresnan, Anette Rosenbach, and Benedikt Szmrecsanyi. “Dative and Genitive Variability in Late Modern English: Exploring Cross-Constructional Variation and Change.” Diachronica 30, no. 3 (2013): 382–419. doi:10.1075/dia.30.3.04wol. The code in the `munge/` directory has been reworked from the original to reflect my own package preferences and coding style. The code in the `R/` directory has only been lightly modified. I wish to thank the authors for making the data and code available to others. My thanks especially to Christoph Wolk for his assistance with any questions or problems I had with the code.  

## Build the Paper
Once the package dependencies have been installed (see below), navigate to the `documents/` directory in the terminal and run `make`. Output will be in .pdf, html, and Word. Alternatively, open `d2-e-proceedings.Rproj` in R Studio and knit `d2-e-proceedings.Rmd` in the `documents/` directory manually as html, as that format has no dependencies. 
 
## Install Dependencies

This repository makes use of the `packrat` to manage package dependencies. In order for `packrat` to install the dependencies, you will need a version of R that is ≥ 3.2.4 ("Very Secure Dishes"). You will also need to be able to build packages locally. Information about the package development prerequisites can be found [here](https://support.rstudio.com/hc/en-us/articles/200486498-Package-Development-Prerequisites). In order to generate the paper using `make`, you will need to have Latex and Word installed. 

Mac users especially may also need to install the gfortran-4.8 package distributed by Simon Urbanek, located (for Mac users) [here](http://r.research.att.com/libs/gfortran-4.8.2-darwin13.tar.bz2)

Once you are able to build packages locally, open `d2-e-proceedings.Rproj` in R Studio and `packrat` should bootstrap itself and install all the necessary packages. If that fails, try the following steps: 

1. Install `packrat`: `install.packages("packrat")`  
2. Turn on `packrat`: `packrat::on()` 
3. Install the dependencies: `packrat::restore()` 

You may have to restart the computer manually in order for the packages to get installed in your packrat library. 

It is also possible to generate the paper without using `packrat`. However, it should be kept in mind that a future update to a package may break the code. First, switch the `load_libraries` option to `TRUE` in `global.dcf`, located in the `config/` directory. Then do the following:

1. Install `ProjectTemplate`: `install.packages("ProjectTemplate")`
2. Load the package: `library(ProjectTemplate)`
3. Let ProjectTemplate do its thing: `load.project()`
4. Install any packages `ProjectTemplate` prompts you to install

(NB: you may want to delete `Rprofile` and the `packrat/` directory before installing `ProjectTemplate` to prevent `packrat` from installing the packages in its library. 

You should then be able to run any of the R files in the project. 