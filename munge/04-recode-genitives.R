#----------------------------------------------------------------------------------------
# File: 04-recode-genitives.R
# Date:  file.info("munge/04-recode-genitives.R")$mtime
# Author: Originally by Wolk et. al. (reworked by Joseph Flanagan)
# email:   joseph.flanagan@helsinki.fi
# Purpose: Create derived and recoded genitive variables from Wolk et. al. 
# Source: http://www.benszm.net/datasets/WBRS-data.zip
#----------------------------------------------------------------------------------------

# Uncomment following if you want to run just this script

# source("munge/01-download-data.R")
# source("munge/02-recreate-raw-data.R")


library(readr)
library(stringr)
library(dplyr)
library(SnowballC)


genitives <- read_csv("data/processed-data/recreated-wolk-raw-archer-genitives.csv")

### ----------------------------------------------------------------------
###  add period values
### ----------------------------------------------------------------------

levels <- c(1649, 1699, 1749, 1799, 1849, 1899, 1949, 1999)
labels <- c("2", "3", "4", "5", "6", "7", "8")


genitives <- genitives %>% mutate(period = cut(year, levels, labels, right = TRUE))



### ----------------------------------------------------------------------
###  Add constituent length variables
### ----------------------------------------------------------------------

# Replace escaped characters with appropriate character length and add constituent length variables

genitives <- genitives %>%
    mutate(possessorLengthCharacters = 
               str_replace_all(genitives$possessorPhrase, "&pound;", ".") %>%
               str_replace("&frac12;", ".") %>% 
               str_trim() %>% 
               nchar(),
           possessumLengthCharacters =
               str_replace_all(genitives$possessumPhrase, "\\bthe\\b", "") %>%
               str_trim() %>%
               str_replace_all("&pound;", ".") %>%
               str_replace_all("&frac12;", ".") %>%
               str_trim() %>%
               nchar()
    )



### ----------------------------------------------------------------------
###  Calculate logged, period-centered weight measures for possessum
### ----------------------------------------------------------------------

### first take log of the possessum Length Characters
genitives <- genitives %>% 
    mutate(log_possessum = log(possessumLengthCharacters))


### then take the mean of the log by period
period_mean_log_possessum <- genitives %>% 
    group_by(period) %>% 
    mutate(log_possessum = log(possessumLengthCharacters)) %>% 
    summarise(period_mean_log_possessum = mean(log_possessum))

### Then join up data frames
genitives <- genitives %>% 
    left_join(period_mean_log_possessum)

### Create new cl variable
genitives <- genitives %>% 
    mutate(clPossessumLength = log_possessum - period_mean_log_possessum)


### ----------------------------------------------------------------------
###  Calculate logged, period-centered weight measures for possessor
### ----------------------------------------------------------------------

### first take log
genitives <- genitives %>% 
    mutate(log_possessor = log(possessorLengthCharacters))

### then take the mean of the log by period
period_mean_log_possessor <- genitives %>% 
    group_by(period) %>% 
    mutate(log_possessor = log(possessorLengthCharacters)) %>% 
    summarise(period_mean_log_possessor = mean(log_possessor))

### Then join up data frames
genitives <- genitives %>% 
    left_join(period_mean_log_possessor)

#### Create new cl variable
genitives <- genitives %>% 
    mutate(clPossessorLength = log_possessor - period_mean_log_possessor)

### ----------------------------------------------------------------------
###  Lower-case and stem PossessorHead variable
### ----------------------------------------------------------------------

genitives$prunedPossessorHead <- wordStem(tolower(genitives$possessorHead))

n_frequent <- table(genitives$prunedPossessorHead)
notkeep <- names(n_frequent[n_frequent < 4])

## Recode values < 4 as OTHER
genitives <- genitives %>%
    mutate(prunedPossessorHead = replace(prunedPossessorHead, prunedPossessorHead %in% notkeep, "OTHER"))


### ----------------------------------------------------------------------
###  Set Factor Levels
### ----------------------------------------------------------------------

# Make s-genitive default so that dative and genitive constructions are more comparable
genitives$realization <- factor(genitives$realization, levels = c("s-genitive", "of-genitive"))


# Set levels for definitenessPossessor
## Get table for frequency
tb <- table(genitives$definitenessPossessor)

## Use table to set factor levels
genitives$definitenessPossessor <- factor(genitives$definitenessPossessor, levels = names(tb[order(tb, decreasing = TRUE)]))

# Set levels for prototypicality 
## Get table
tb <- table(genitives$prototypicality)

## Use table to set factor levels
genitives$prototypicality <- factor(genitives$prototypicality, levels = names(tb[order(tb, decreasing = TRUE)]))

### ----------------------------------------------------------------------
###  Clean-up
### ----------------------------------------------------------------------

genitives <- genitives %>% select(-c(log_possessor, log_possessum, period_mean_log_possessor, period_mean_log_possessum))

rm(levels, labels, period_mean_log_possessor, period_mean_log_possessum)
