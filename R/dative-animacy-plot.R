#----------------------------------------------------------------------------------------
# File: dative-animacy-plot
# Date:  2016-03-18
# Author: Wolk et. al. 
# email:   joseph.flanagan@helsinki.fi
# Purpose: 
#----------------------------------------------------------------------------------------
# Uncomment the following if running interactively

# library(ProjectTemplate)

# load.project()

library(ggplot2)

datives.time  <- ggplot(datives, aes(period2year(period))) +
    xlab("real time") + theme_bw()

dative.animacy.plot <- datives.time + 
    ## add lines and points for realization proportion
    geom_line(aes(y = abs(as.numeric(realization) - 2),
                  linetype = animacyRecipient.reduced), size = 1,
              stat="summary", fun.y = mean) + 
    geom_point(aes(y = abs(as.numeric(realization) - 2),
                   shape = animacyRecipient.reduced),
               size = 3, stat = "summary", fun.y = mean) +
    ## configure axis labels, linetypes, shapes, and legend
    ylab("Proportion DO datives") +
    scale_shape_discrete("Animacy", 
                         breaks= c("animate", "collective", "inanimate"),
                         labels= c("animate", "collective", "inanimate"), solid = FALSE) +
    scale_linetype_discrete("Animacy", 
                            breaks= c("animate","collective", "inanimate"),
                            labels= c("animate", "collective", "inanimate"))