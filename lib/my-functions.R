#----------------------------------------------------------------------------------------
# File: my-functions.R
# Date:  2015-10-10
# Author: Wolk et. al 
# email:   joseph.flanagan@helsinki.fi
# Purpose: Functions to create derived variables from Wolk et. al
# Source: http://www.benszm.net/datasets/WBRS-data.zip
#----------------------------------------------------------------------------------------


# Function used in converting archer period to period midpoint in years
period2year <- function(period) 
    as.numeric(factor(period)) * 50 + 1625

# Function for splitting data into 50 bins, identified by midpoints. Used in weight-plots. 
binning <- function(data, nbins=50){
    bins <- cut(data, nbins)
    binmeans <- sapply (levels (bins), function(x) mean(data[bins == x]))
    return (binmeans[as.numeric(bins)])
}


### ----------------------------------------------------------------------
###  Functions used by Wolk et. al. but ultimately replaced by alternatives
### ----------------------------------------------------------------------

# Function for replacing special characters with appropriate character length. No longer used. 
cleanSpecialCharacters <- function(strings) {
    strings <- gsub(x = strings, pattern = "&pound;", replacement = ".")
    strings <- gsub(x = strings, pattern = "&frac12;", replacement = ".")
    return(strings)
}

# Function for removing determiner in gentives data. No longer used. 
removeDet <- function(pum) {
    pumWords <- strsplit(as.character(pum), " ")
    return(unlist(lapply(pumWords, function(x) {
        if (tolower(x[1]) %in% c("the"))
            paste(x[2:length(x)], collapse = " ")
        else paste(x, collapse = " ")
    })))
}


# Function for creating period-centered weigh measures. Used in recode-datives.R and recode-genitives.R No longer used. 
periodmeans <- function(values, periods){
    sapply(unique(periods), function(x){mean(values[periods == x])})
}


# Function for normalising genitive data. Used in genitive plots. No longer used 
fun.normalize.genitives <- function(periodcolumn) {
    (length(periodcolumn) / 
         rowSums(archercounts[archercounts$variety == "British", 
                              c("News", "Letters")])[periodcolumn[1] - 1]
    )* 10000
}


# Function for normalising datives data. Used in dative plots. No longer used (requires reshape)
fun.normalize.datives <- function(periodcolumn){
    (length(periodcolumn) /
         t(recast(archercounts[archercounts$years != "total",
                               c("total", "variety", "period")],
                  ~ period, id.var = c("variety", "period"),
                  fun.aggregate = sum)[1, 2 : 8])[periodcolumn[1] - 1]
    ) * 10000
}



# Function for fixing p-values reported in X table
fixp <- function(x, dig=3){
    x <- as.data.frame(x)
    
    if(substr(names(x)[ncol(x)],1,2) != "Pr")
        warning("The name of the last column didn't start with Pr. This may indicate that p-values weren't in the last row, and thus, that this function is inappropriate.")
    x[,ncol(x)] <- round(x[,ncol(x)], dig)
    for(i in 1:nrow(x)){
        if(x[i,ncol(x)] == 0)
            x[i,ncol(x)] <- paste0("< .", paste0(rep(0,dig-1), collapse=""), "1")
    }
    
    x
}